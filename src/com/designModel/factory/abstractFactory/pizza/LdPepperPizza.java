package com.designModel.factory.abstractFactory.pizza;

public class LdPepperPizza extends Pizza {
    @Override
    public void prepare() {
        setName("伦敦胡椒披萨");
        System.out.println("伦敦胡椒披萨 原材料准备中");
    }
}
