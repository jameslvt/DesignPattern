package com.designModel.factory.abstractFactory.pizza;

public class BjCheesePizza extends Pizza {
    @Override
    public void prepare() {
        setName("北京奶酪披萨");
        System.out.println("北京奶酪披萨 原材料准备");
    }
}
