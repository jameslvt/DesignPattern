package com.designModel.factory.abstractFactory.order;

import com.designModel.factory.abstractFactory.pizza.Pizza;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class OrderPizza {

    AbstractFactory abstractFactory;

    public OrderPizza(AbstractFactory abstractFactory) {
        setAbstractFactory(abstractFactory);
    }

    public void setAbstractFactory(AbstractFactory abstractFactory){
        Pizza pizza = null;
        this.abstractFactory = abstractFactory;

        do {
            String orderType = getType();
            // factory 可能是北京的工厂子类，也可能是伦敦的工厂子类
            pizza = abstractFactory.createPizza(orderType);
            if (pizza != null) {
                pizza.bake();
                pizza.cut();
                pizza.box();
            }else{
                System.out.println("暂未发现该披萨");
            }
        }while (true);
    }


    // 写一个方法，可以获取客户希望订购的披萨种类
    private String getType() {
        try {
            BufferedReader strin = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("input pizza 种类:");
            String str = strin.readLine();
            return str;
        } catch (
                IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
