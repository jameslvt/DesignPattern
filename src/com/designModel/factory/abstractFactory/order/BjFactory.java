package com.designModel.factory.abstractFactory.order;

import com.designModel.factory.abstractFactory.pizza.*;

public class BjFactory implements AbstractFactory {

    @Override
    public Pizza createPizza(String orderType) {
        System.out.println("~使用的是抽象工厂模式~");
        Pizza pizza = null;
        if(orderType.equals("cheese")) {
            pizza = new BjCheesePizza();
        } else if (orderType.equals("pepper")){
            pizza = new BjPepperPizza();
        }
        return pizza;
    }
}
