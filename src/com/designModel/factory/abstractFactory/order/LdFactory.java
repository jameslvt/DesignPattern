package com.designModel.factory.abstractFactory.order;

import com.designModel.factory.abstractFactory.pizza.LdCheesePizza;
import com.designModel.factory.abstractFactory.pizza.LdPepperPizza;
import com.designModel.factory.abstractFactory.pizza.Pizza;

public class LdFactory implements AbstractFactory {

    @Override
    public Pizza createPizza(String orderType) {
        System.out.println("~使用的是抽象工厂模式~");
        Pizza pizza = null;
        if(orderType.equals("cheese")) {
            pizza = new LdCheesePizza();
        } else if (orderType.equals("pepper")){
            pizza = new LdPepperPizza();
        }
        return pizza;
    }
}
