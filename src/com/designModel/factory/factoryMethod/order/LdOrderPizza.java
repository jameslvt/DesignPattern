package com.designModel.factory.factoryMethod.order;

import com.designModel.factory.factoryMethod.pizza.LdCheesePizza;
import com.designModel.factory.factoryMethod.pizza.LdPepperPizza;
import com.designModel.factory.factoryMethod.pizza.Pizza;

public class LdOrderPizza extends OrderPizza {
    @Override
    Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if(orderType.equals("cheese")){
            pizza = new LdCheesePizza();
        }else if(orderType.equals("pepper")){
            pizza = new LdPepperPizza();
        }
        return pizza;
    }
}
