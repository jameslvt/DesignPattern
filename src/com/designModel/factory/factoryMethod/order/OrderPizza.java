package com.designModel.factory.factoryMethod.order;

import com.designModel.factory.factoryMethod.pizza.Pizza;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class OrderPizza {

    public OrderPizza() {
        Pizza pizza = null;
        String orderType ;//订购披萨的类型

        do{
            orderType = getType();
            pizza = createPizza(orderType); //抽象方法，由我们工厂子类完成
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }while (true);
    }

    //定义一个抽象方法，createPizza，让各个工厂自己实现
    abstract Pizza createPizza(String orderType);

    private String getType(){
        BufferedReader str = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("input 披萨种类");
        String stri = null;
        try {
            stri = str.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stri;
    }
}


