package com.designModel.factory.factoryMethod.pizza;

import com.oracle.webservices.internal.api.databinding.DatabindingMode;

/**
 * 抽象方法pizza
 * @date: 2020/8/26
 * @author wangth
 * @title: Pizza
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public abstract class Pizza {
    protected String name;

    //准备原材料，不同的披萨不一样，做成抽象方法
    public abstract void prepare();

    public void bake(){
        System.out.println(name + "  bake");
    }

    public void cut(){
        System.out.println(name + " cut");
    }

    //打包
    public void box(){
        System.out.println(name + " box");
    }

    public void setName(String name) {
        this.name = name;
    }
}
