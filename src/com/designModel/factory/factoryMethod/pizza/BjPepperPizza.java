package com.designModel.factory.factoryMethod.pizza;

public class BjPepperPizza extends Pizza {
    @Override
    public void prepare() {
        setName("北京胡椒披萨");
        System.out.println("北京胡椒披萨 原材料准备中");
    }
}
