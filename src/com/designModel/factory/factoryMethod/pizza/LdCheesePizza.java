package com.designModel.factory.factoryMethod.pizza;

public class LdCheesePizza extends Pizza {
    @Override
    public void prepare() {
        setName("伦敦奶酪披萨");
        System.out.println("伦敦奶酪披萨 原材料准备");
    }
}
