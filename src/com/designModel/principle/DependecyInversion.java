package com.designModel.principle;

import java.net.InterfaceAddress;

/**
 * 依赖倒置原则
 * @date: 2020/8/19
 * @author wangth
 * @title: DependecyInversion
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class DependecyInversion {

    public static void main(String[] args) {
        Person person = new Person();
        person.recevier(new Email());
        person.recevier(new Weixin());
    }


}

interface IReceiver {
    public String getInfo();
}

class Email implements IReceiver{
    public String getInfo(){
        return "电子信息：email";
    }
}


class Weixin implements IReceiver{
    public String getInfo(){
        return "Hello WeChat";
    }
}
class Person{
    //对接口的依赖
    public void recevier(IReceiver receiver){
        System.out.println(receiver.getInfo());
    }
}