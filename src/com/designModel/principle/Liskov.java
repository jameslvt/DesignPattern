package com.designModel.principle;

/**
 * 里氏替换原则，遵循几个点：
 * 1、所有引用基类的地方必须能够透明的使用其子类的对象
 * 2、子类中尽量不要重写父类的方法
 * 3、适当情况下，我们可通过聚合，组合，依赖来解决问题
 * @date: 2020/8/19
 * @author wangth
 * @title: Liskov
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Liskov {
    public static void main(String[] args) {

    }
}

//创建一个更加基础的类，把更加基础的方法和成员写到base类中
class Base{

}

class A extends Base{
    public int fun1(int a,int b){
        return a-b;
    }
}

class B extends  Base{

    A a = new A();

    // 这里重写了a的方法，无意识
    public int fun2(int a,int b){
        return a + b;
    }

    //我们若想使用A的方法，必须通过组合的方式进行
    public int fun3(int a, int b){
        return this.a.fun1(a,b);
    }
}