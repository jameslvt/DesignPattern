package com.designModel.principle;


/**
 * 依赖传递关系的三种方式
 * @date: 2020/8/19
 * @author wangth
 * @title: DependecyPass
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class DependecyPass {
    public static void main(String[] args) {
        ChangHong changHong = new ChangHong();
        OpenAndClose openAndClose = new OpenAndClose();
        openAndClose.open(changHong);

        //方式二，通过构造器依赖
       /* OpenAndClose openAndClose = new OpenAndClose(changHong);
        openAndClose.open();*/
        //3.set方式依赖
        /*openAndClose.setTv(changHong);
        openAndClose.open();*/
    }
}

class ChangHong implements ITv{

    @Override
    public void play() {
        System.out.println("打开长虹电视机");
    }
}

//1.通过接口实现依赖

interface ITv{
    public void play();
}

interface IOpenAndClose{
    //抽象方法，接收接口
    public void open(ITv tv);
}

class OpenAndClose implements IOpenAndClose{

    @Override
    public void open(ITv tv) {
        tv.play();
    }
}


//2.通过构造器方法传递依赖

/*interface ITv{
    public void play();
}

interface IOpenAndClose{
    public void open();
}

class OpenAndClose implements IOpenAndClose{

    //成员
    public ITv tv;

    //构造器
    public OpenAndClose(ITv tv) {
        this.tv = tv;
    }

    @Override
    public void open() {
        this.tv.play();
    }
}*/


//3.通过setter方法传递依赖
/*
interface ITv{
    public void play();
}

interface IOpenAndClose{
    public void setTv(ITv tv);

    public void open();

}

class OpenAndClose implements IOpenAndClose{
    public ITv tv;

    @Override
    public void setTv(ITv tv) {
        this.tv = tv;
    }

    @Override
    public void open() {
        this.tv.play();
    }


}*/