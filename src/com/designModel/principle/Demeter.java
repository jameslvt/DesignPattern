package com.designModel.principle;

/**
 * 迪米特法则
 * 1、最少知道原则：一个类对自己依赖的类知道的越少越好，即：对于被依赖的类不管多么复杂，都尽量降逻辑封装在类的内部，对外除了提供public方法，
 *    不对外泄漏任何信息
 * 2、直接朋友：每个对象都会与其他的对象有耦合关系，只要两个对象之间有耦合关系，我们就是说这两个对象之间是朋友关系，耦合的方式很多，依赖，关联，组合，聚合
 *    其中，我们称出现成员变量，方法参数，方法返回值中的类为直接的朋友，而出现的局部变量类不是直接朋友。
 * @date: 2020/8/20
 * @author wangth
 * @title: Demeter
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Demeter {
    //设计模式六大原则：
    /*
    * 1、单一原则
    * 2、接口隔离原则
    * 3、开闭原则
    * 4、里氏替换原则
    * 5、迪米特法则
    * 6、依赖倒置原则
    * */
}
