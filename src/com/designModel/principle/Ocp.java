package com.designModel.principle;

/**
 * 开闭原则：最基本，最重要的原则
 * 1、模块和函数应该对扩展开放（对提供方），对修改关闭（对使用方）
 * @date: 2020/8/20
 * @author wangth
 * @title: Ocp
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Ocp {
    public static void main(String[] args) {
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.drawShap(new Rectangle());
        graphicEditor.drawShap(new Cricle());
        graphicEditor.drawShap(new Triangle());
    }
}

//这是一个绘制图形的类【使用方】
class GraphicEditor{
    public void drawShap(Shape shape){
        shape.draw();
    }
}



abstract class Shape{
    int type;

    //抽象的方法
    public abstract void draw();
}

//提供方
class Rectangle extends Shape{
    public Rectangle() {
        super.type = 1;
    }

    @Override
    public void draw() {
        System.out.println("绘制矩形");
    }
}

class Cricle extends Shape{

    public Cricle() {
        super.type = 2;
    }

    @Override
    public void draw() {
        System.out.println("绘制圆形");

    }
}

class Triangle extends Shape{

    public Triangle() {
        super.type = 3;
    }

    @Override
    public void draw() {
        System.out.println("绘制三角形");
    }
}