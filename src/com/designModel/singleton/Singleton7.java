package com.designModel.singleton;

/**
 * 静态内部类，推荐使用
 * 1、这种方式采用类类装载机制来保证初始化时候只有一个线程
 * 2、静态内部类方式在Singleton类被装载的时并不会立即实例化，只有在使用的时候才会被实例化
 * 3、类的静态属性只会在第一次加载类的时候初始化，jvm帮助我们保证了线程安全性，在类进行初始化的时候，别的线程是无法进入的。
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Singleton7 {
    //私有化，防止外部new
    private Singleton7() {

    }

    private  static Singleton7 instance;

    //静态内部类
    private static class SingletonInstance{
        private static final Singleton7 INSTANCE = new Singleton7();
    }

    //提供一个静态公有方法，直接返回实例对象
    public static Singleton7 getInstance(){
        return SingletonInstance.INSTANCE;
    }
}