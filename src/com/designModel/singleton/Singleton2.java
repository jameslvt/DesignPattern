package com.designModel.singleton;

/**
 * 饿汉式写法（静态代码块）
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Singleton2 {

    //私有化，防止外部new
    private Singleton2() {

    }

    private final static Singleton2 instance;

    static {
        instance = new Singleton2();
    }

    public static Singleton2 getInstance(){
        return instance;
    }
}