package com.designModel.singleton;

/**
 * 懒汉式写法（多线程-双重判断-同步代码块）
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Singleton5 {

    //私有化，防止外部new
    private Singleton5() {

    }

    //volatile 防止指令重排序
    private static volatile Singleton5 instance;

    //当我们使用的时候我们才去创建该对象
    public static Singleton5 getInstance(){
        if (instance == null) {
            synchronized (Singleton5.class){
                if (instance == null) {
                    instance = new Singleton5();
                }
            }
        }
        return instance;
    }
}