package com.designModel.singleton;

import sun.security.jca.GetInstance;

/**
 * 饿汉式写法
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Singleton1 {

    private final static  Singleton1 instance = new Singleton1();

    private Singleton1() {

    }

    public static Singleton1 getInstance(){
        return instance;
    }
}