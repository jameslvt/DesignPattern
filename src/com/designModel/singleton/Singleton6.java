package com.designModel.singleton;

/**
 * 枚举类型，推荐使用
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public enum  Singleton6 {
    INSTANCE;
}