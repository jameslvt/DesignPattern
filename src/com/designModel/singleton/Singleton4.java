package com.designModel.singleton;

/**
 * 懒汉式写法（多线程）
 * @date: 2020/8/20
 * @author wangth
 * @title: Singleton1
 * @version: 1.0
 * @description:
 * update_version: update_date: update_author: update_note:
 */
public class Singleton4 {

    //私有化，防止外部new
    private Singleton4() {

    }

    private static Singleton4 instance;

    //当我们使用的时候我们才去创建该对象
    public static synchronized Singleton4 getInstance(){
        if (instance == null) {
            instance = new Singleton4();
        }
        return instance;
    }
}